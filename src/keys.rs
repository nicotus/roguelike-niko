/*
Sisältää kaikki näppäimistöllä tehtävät komennot esimerkiksi:
    - Liike
    - Exittaus
    - Fullscreen
*/

extern crate tcod;

pub mod keyboard {
    use tcod::console::*;
    use crate::{Object, PLAYER, player_move_or_attack};
    use crate::PlayerAction::{self, *};
    use crate::maps::maps_render::Map;
    use tcod::input::Key;
    use tcod::input::KeyCode::*;

    pub fn handle_keys(root: &mut Root, objects: &mut [Object], map: &Map) -> PlayerAction {

        let key = root.wait_for_keypress(true);
        let player_alive = objects[PLAYER].alive;
        match (key, player_alive) {
            // Movement
            (Key { code: Up, .. }, true) => {
                player_move_or_attack(0, -1, map, objects);
                TookTurn
            }
            (Key { code: Down, .. }, true) => {
                player_move_or_attack(0, 1, map, objects);
                TookTurn
            }
            (Key { code: Left, .. }, true) => {
                player_move_or_attack(-1, 0, map, objects);
                TookTurn
            }
            (Key { code: Right, .. }, true) => {
                player_move_or_attack(1, 0, map, objects);
                TookTurn
            }

            // Fulscreen
            (Key { code: Enter, alt: true, .. }, _,) => {
                // Alt + Enter: toggle fullscreen
                let fullscreen = root.is_fullscreen();
                root.set_fullscreen(!fullscreen);
                DidntTakeTurn
            }

            // Exit
            (Key { code: Escape, .. }, _) => return Exit, // exit game

            _ => DidntTakeTurn,
        }
    }
}
