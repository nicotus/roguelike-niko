/*
Tällä hetkellä täällä on myös Object struct, mikä sisältää kaikki objektit, mitä
kentälle renderöidään (render_all() löytyy render moduulista). Object structille on
implementoitu kolme metodia new(), move_by() ja draw().
*/
extern crate tcod;

pub mod object_mod {

    use tcod::console::*;
    use tcod::colors::{Color};
    use crate::maps::maps_render::{Map, is_blocked};

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub struct Fighter {
        pub max_hp: i32,
        pub hp: i32,
        pub defense: i32,
        pub power: i32,
    }

    #[derive(Clone, Copy, Debug, PartialEq)]
    pub struct Ai;

    pub struct Object {
        pub x: i32,
        pub y: i32,
        pub char: char,
        pub color: Color,
        pub name: String,
        pub blocks: bool,
        pub alive: bool,
        pub fighter: Option<Fighter>,
        pub ai: Option<Ai>,
    }

    impl Object {
        pub fn new(x: i32, y: i32, char: char, name: &str, color: Color, blocks: bool) -> Self {
            Object {
                x: x,
                y: y,
                name: name.into(),
                char: char,
                color: color,
                blocks: blocks,
                alive: false,
                fighter: None,
                ai: None,
            }
        }

        pub fn move_by(id: usize, dx: i32, dy: i32, map: &Map, objects: &mut [Object]) {
            let (x, y) = objects[id].pos();
            if !is_blocked(x + dx, y + dy, map, objects) {
                objects[id].set_pos(x + dx, y + dy);
            }
        }

        pub fn draw(&self, con: &mut Console) {
            con.set_default_foreground(self.color);
            con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
        }

        pub fn pos(&self) -> (i32, i32) {
            (self.x, self.y)
        }

        pub fn set_pos(&mut self, x:i32, y: i32) {
            self.x = x;
            self.y = y;
        }
    }
}
