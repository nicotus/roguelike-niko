/*
Main.rs ajaa pää while loopia, mikä toimi peli loopppina. Eli kun peli exittaa
loopista, se loppuu.

Kartta luodaan make_map() funktiolla, mikä myös määrittelee pelaajlle aloitus koordinaatit
*/

extern crate tcod;
mod keys;
mod maps;
mod object;
mod render;

use tcod::console::*;
use tcod::colors::{self};
use crate::object::object_mod::{Object, Fighter, Ai};
use crate::maps::maps_render::{Map, MAP_WIDTH, MAP_HEIGHT, make_map};
use tcod::map::{Map as FovMap};

// Näytön koko
const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;

// FPS rajoitus
const LIMIT_FPS: i32 = 20;

// Player asetetaan oletetuksena 0, joten koodista tulee selkeempää
const PLAYER: usize = 0;

// Vuorojen määrittelyä
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PlayerAction {
    TookTurn,
    DidntTakeTurn,
    Exit,
}

fn player_move_or_attack(dx: i32, dy: i32, map: &Map, objects: &mut [Object]){
    let x = objects[PLAYER].x + dx;
    let y = objects[PLAYER].y + dy;

    let target_id = objects.iter().position(|object| object.pos() == (x, y));

    match target_id {
        Some(target_id) => {
            println!("The {} laughs at your puny efforts to attack him!",  objects[target_id].name);
        }
        None => {
            Object::move_by(PLAYER, dx, dy, map, objects);
        }
    }
}

fn main() {
    let mut root = Root::initializer()
        .font("arial10x10.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("Rust/libtcod tutorial")
        .init();

    tcod::system::set_fps(LIMIT_FPS);

    // Luodaan uusi offscreen konsoli, mikä tullaan liittäämään root konsoliin.
    // Tällä tavalla voidaan esimerkiksi luoda minimap ja muita erilaisia GUI juttuja.
    let mut con = Offscreen::new(SCREEN_WIDTH, SCREEN_HEIGHT);

    // Luodaan pelaaja ja npc ja nämä laitetaan objectsin sisälle pelaaja on objects[0]
    let mut player = Object::new(0, 0, '@', "player", colors::WHITE, true);
    player.alive = true;
    player.fighter = Some(Fighter {
        max_hp: 30,
        hp: 30,
        defense: 2,
        power: 5,
    });

    let mut objects = vec![player];

    // Luodaan kartta ja annetaan pelaajallle aloitus koordinaatit
    let mut map = make_map(&mut objects);

    // FOV: kerrotaan mitkä palaset blokkaavat näkyvyyttä
    let mut fov_map = FovMap::new(MAP_WIDTH, MAP_HEIGHT);
    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            fov_map.set(x, y,
                 !map[x as usize][y as usize].block_sight,
                 !map[x as usize][y as usize].blocked);
        }
    }

    let mut previous_player_position = (-1, -1);

    // Tämä while loop tullaan toteuttamaan nyt 20 kertaa sekunnissa
    while !root.window_closed() {
        // Putsaa edellisen framen piirokset
        con.clear();

        /*
         Piirtää kaikki objektit ja render_all() funktiossa on blit, mikä
         suorittaa blittauksen rootti konsoliin. root.flush() sitten piirtää
         kaiken näytölle.

         Tämän lisäksi render_all() suoriitaa fovin uudelleen laskennan. Tämä laskee
         sen vain uudelleen jos esimerkiksi pelaajan sijainissa on tapahtunut muutos.
        */
        let fov_recompute = previous_player_position != (objects[PLAYER].x, objects[PLAYER].y);
        render::rendering::render_all(&mut root, &mut con, &objects, &mut map, &mut fov_map, fov_recompute);

        root.flush();

        previous_player_position = objects[PLAYER].pos();

        // Näppäimistön toiminta ja exittaa jos tarve
        let player_action = keys::keyboard::handle_keys(&mut root, &mut objects, &map);
        if player_action == PlayerAction::Exit {
            break
        }

        if objects[PLAYER].alive && player_action != PlayerAction::DidntTakeTurn {
            for object in &objects {
                // only if object is not player
                if (object as *const _) != (&objects[PLAYER] as *const _) {
                    println!("The {} growls!", object.name );
                }
            }
        }
    }
}
