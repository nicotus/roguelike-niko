/*
Render moduulissa hoidetaan objetktin renderöinti funktiolla render_all(), tämä
saa muuttujia kuten root, con, objektit, map, fov_map...
Kaikki mitä pitää renderöidä.

render_all() funktio hoitaa myös näkyvyyden määrittelyn pelaajalle, sen takia seinien ja lattian
värit on määritelty tässä moduulissa.
*/

extern crate tcod;

pub mod rendering {
    use tcod::console::*;
    use tcod::colors::Color;
    use tcod::map::{Map as FovMap, FovAlgorithm};
    use crate::{Object, PLAYER};
    use crate::maps::maps_render::{Map, MAP_WIDTH, MAP_HEIGHT};

    // Luolaston luonti parametreja
    const COLOR_DARK_WALL: Color = Color { r: 0, g: 0, b: 100 };
    const COLOR_LIGHT_WALL: Color = Color { r: 130, g: 110, b: 50};
    const COLOR_DARK_GROUND: Color = Color { r: 50, g: 50, b: 150 };
    const COLOR_LIGHT_GROUND: Color = Color { r: 200, g: 180, b: 50};

    // Parametreja FOV:lle (Field of view)
    const FOV_ALGO: FovAlgorithm = FovAlgorithm::Basic;
    const FOV_LIGHT_WALLS: bool = true;
    const TORCH_RADIUS: i32 = 10;

        pub fn render_all(root: &mut Root, con: &mut Offscreen, objects: &[Object], map: &mut Map, fov_map: &mut FovMap, fov_recompute: bool) {
            // Suoritaa fovin uudelleen laskennan, vain jos on tapahtunut muutosta, kuten pelaajan liikettä
            if fov_recompute {
                let player = &objects[PLAYER];
                fov_map.compute_fov(player.x, player.y, TORCH_RADIUS, FOV_LIGHT_WALLS, FOV_ALGO);
            }
            // Käy läpi kaikki palat ja laittaa näille taustavärinS
            for y in 0..MAP_HEIGHT {
                for x in 0..MAP_WIDTH {
                    let visible = fov_map.is_in_fov(x, y);
                    let wall = map[x as usize][y as usize].block_sight;
                    let color = match (visible, wall){
                            // outside of field of view:
                            (false, true) => COLOR_DARK_WALL,
                            (false, false) => COLOR_DARK_GROUND,
                            // inside fov
                            (true, true) => COLOR_LIGHT_WALL,
                            (true, false) => COLOR_LIGHT_GROUND,
                    };
                    let explored = &mut map[x as usize][y as usize].explored;
                    if visible {
                        // since it's visible, explore it
                        *explored = true;
                    }
                    if *explored {
                        // show explored tiles only (any visible tile is esplored already)
                        con.set_char_background(x, y, color, BackgroundFlag::Set);
                    }
                }
            }

            for object in objects {
                if fov_map.is_in_fov(object.x, object.y) {
                    object.draw(con);
                }
            }
            // Tässä käytännössä blitataan koko con rootti konsoliin, mikä sitten piiretään näytölle.
            blit(con, (0, 0), (MAP_WIDTH, MAP_HEIGHT), root, (0, 0), 1.0, 1.0);
        }
}
