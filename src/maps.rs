/*
Sisältää kaiken kartan luontiin esimerkiksi:
    - Tile structin, mikä määrittelee onko jokin kohta blocked vai ei
    - make_map() funktio minkä avulla luodaan kartta
    - Rect structin, mikä määrittelee huoneelle mitat.
*/

extern crate tcod;
extern crate rand;

pub mod maps_render {
    use std::cmp;
    use crate::{Object, PLAYER, Fighter, Ai};
    use rand::Rng;
    use tcod::colors::{self};

    // Kartan koko
    pub const MAP_WIDTH: i32 = 80;
    pub const MAP_HEIGHT: i32 = 45;

    // Huoneiden kokoon ja määrään liittyvät rajoitukset
    const ROOM_MAX_SIZE: i32 = 10;
    const ROOM_MIN_SIZE: i32 = 6;
    const MAX_ROOMS: i32 = 30;

    const MAX_ROOMS_MONSTERS: i32 = 3;

    #[derive(Clone, Copy, Debug)]
    struct Rect {
        x1: i32,
        y1: i32,
        x2: i32,
        y2: i32,
    }

    // Rect on huoneen mittojen ja luomiseen käytettävä struct
    impl Rect {
        // Määritellään uusi huoneen mitat
        pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
            Rect { x1: x, y1: y, x2: x + w, y2: y + h }
        }
        // Pelaajan koordinaatit mitkä ovat ensimmäisen huoneen keskellä
        pub fn center(&self) -> (i32, i32) {
            let center_x = (self.x1 + self.x2) / 2;
            let center_y = (self.y1 + self.y2) / 2;
            (center_x, center_y)
        }

        // Funktio mikä tarkistaa osuuko suorakulmiot
        pub fn intersects_with(&self, other: &Rect) -> bool {
            // Palauttaa true, jos huoneet ovat päälekkäin
            (self.x1 <= other.x2) && (self.x2 >= other.x1) &&
                (self.y1 <= other.y2) && (self.y2 >= other.y1)
        }
    }

    // Luodaan huone, funktio tarvitsee määritellyn Rectin, millä on huoneen mitat.
    fn create_room(room: Rect, map: &mut Map) {
        for x in (room.x1 + 1)..room.x2 {
            for y in (room.y1 + 1)..room.y2 {
                map[x as usize][y as usize] = Tile::empty();
            }
        }
    }

    // Luodaan vaakaasuntaan kulkeva tunneli
    fn create_h_tunnel(x1: i32, x2: i32, y: i32, map: &mut Map) {
        for x in cmp::min(x1, x2)..(cmp::max(x1, x2) + 1) {
            map[x as usize][y as usize] = Tile::empty();
        }
    }

    // Luodaan pystysuuntaan kulkeva tunneli
    fn create_v_tunnel(y1: i32, y2: i32, x: i32, map: &mut Map) {
        for y in cmp::min(y1, y2)..(cmp::max(y1, y2) + 1) {
            map[x as usize][y as usize] = Tile::empty();
        }
    }

    // Tile struct määrittelee onko jokin tyhjää (lattia) tai seinää
    #[derive(Clone, Copy, Debug)]
    pub struct Tile {
        pub blocked: bool,
        pub block_sight: bool,
        pub explored: bool,
    }

    impl Tile {
        pub fn empty() -> Self {
            Tile{blocked: false, explored: false, block_sight: false}
        }

        pub fn wall() -> Self {
            Tile{blocked: true, explored: false, block_sight: true}
        }
    }

    pub type Map = Vec<Vec<Tile>>;

    fn place_objects(room: Rect, map: &Map, objects: &mut Vec<Object>) {

        // Satunnainen määrä hirviöitä
        let num_monsters = rand::thread_rng().gen_range(0, MAX_ROOMS_MONSTERS + 1);

        for _ in 0..num_monsters {
            // Satunnainen paikka hirviölle
            let x = rand::thread_rng().gen_range(room.x1 + 1, room.x2);
            let y = rand::thread_rng().gen_range(room.y1 + 1, room.y2);

            if !is_blocked(x, y, map, objects) {
            let mut monster = if rand::random::<f32>() < 0.8 { // 80% mahdollisuus saada örkki
                // luo örkki
                let mut orc = Object::new(x, y, 'o', "orc", colors::DESATURATED_GREEN, true);
                orc.fighter = Some(Fighter {
                    max_hp: 10,
                    hp: 10,
                    defense: 0,
                    power: 3,
                });
                orc.ai = Some(Ai);
                orc
            }
            else {
                // luo troll
                let mut troll = Object::new(x, y, 'T', "troll", colors::DARKER_GREEN, true);
                troll.fighter = Some(Fighter {
                    max_hp: 16,
                    hp: 16,
                    defense: 1,
                    power: 4,
                });
                troll.ai = Some(Ai);
                troll
            };

            monster.alive = true;
            objects.push(monster);
        }
        }
    }

    pub fn is_blocked(x: i32, y: i32, map: &Map, objects: &[Object]) -> bool {
        if map[x as usize][y as usize].blocked {
            return true;
        }
        objects.iter().any(|object| {
            object.blocks && object.x == x && object.y == y
        })
    }

    pub fn make_map(objects: &mut Vec<Object>) -> Map {
        // Täytetään kenttä "blocked" paloilla
        let mut map = vec![vec![Tile::wall(); MAP_HEIGHT as usize]; MAP_WIDTH as usize];

        let mut rooms = vec![];

        for _ in 0..MAX_ROOMS {
            // random width and height
            let w = rand::thread_rng().gen_range(ROOM_MIN_SIZE, ROOM_MAX_SIZE + 1);
            let h = rand::thread_rng().gen_range(ROOM_MIN_SIZE, ROOM_MAX_SIZE + 1);
            // random position without going out of the boundaries of the map
            let x = rand::thread_rng().gen_range(0, MAP_WIDTH - w);
            let y = rand::thread_rng().gen_range(0, MAP_HEIGHT - h);

            let new_room = Rect::new(x, y, w, h);

            // run through the other rooms and see if they intersect with this one
            // The iter method returns an iterator — a value we can query for each item in the vector
            // any. metodi ajaa koodin jokaiselle itemille rooms vektorissa
            // Heti kun se kohtaa falsen, se keskeyttää, jos ei tule keskeytystä voidaan luoda huone
            let failed = rooms.iter().any(|other_room| new_room.intersects_with(other_room));

            if !failed {
                create_room(new_room, &mut map);

                // Laitetaan hirviöt huoneisiin
                place_objects(new_room, &map, objects);

                let (new_x, new_y) = new_room.center();

                // Pelaajan aloitus koordinaatit laitetaan vain, jos rooms vektori on tyhjä
                if rooms.is_empty() {
                    // Täällä starting_position on pelaajan aloitus koordinaatit
                    objects[PLAYER].set_pos(new_x, new_y);
                }
                else {
                    // all rooms after the first:
                    // conncet it to the previous room with a tunnel

                    // center coordinates of the previous room
                    let (prev_x, prev_y) = rooms[rooms.len() - 1].center();

                    // draw a coin (random bool value -- either true of false)
                    if rand::random() {
                        // first move horizontally, then vertically
                        create_h_tunnel(prev_x, new_x, prev_y, &mut map);
                        create_v_tunnel(prev_y, new_y, new_x, &mut map);
                    }
                    else {
                        // first move vertically, then horizontally
                        create_v_tunnel(prev_y, new_y, prev_x, &mut map);
                        create_h_tunnel(prev_x, new_x, new_y, &mut map);
                    }
                }

                // finallly, append the new room to the list
                rooms.push(new_room);
            }
        }

        map
    }

}
